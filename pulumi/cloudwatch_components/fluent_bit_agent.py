from pulumi import ResourceOptions
from pulumi_eks import Cluster
import pulumi_kubernetes as kubernetes

# FluentBit est l'agent responsable des groupes de journaux application, dataplane et host disponible sur cloudwatch.
# Sans cette composante, les journaux des applications ne sont pas accessible sur cloud watch.
# Source: https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/latest/k8s-deployment-manifest-templates/deployment-mode/daemonset/container-insights-monitoring/fluent-bit/fluent-bit-compatible.yaml
# Traduit avec https://www.pulumi.com/kube2pulumi/
def FluentBitAgent(cluster: Cluster, opts: ResourceOptions):
    amazon_cloudwatch_cluster_info_config_map = kubernetes.core.v1.ConfigMap("amazon_cloudwatchCluster_infoConfigMap",
        api_version="v1",
        data={
            "cluster.name": cluster.core.cluster.name.apply(lambda name: name),
            "logs.region": "ca-central-1",
            "http.server": "On",
            "http.port": "2020",
            "read.head": "Off",
            "read.tail": "On",
        },
        kind="ConfigMap",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="fluent-bit-cluster-info",
            namespace="amazon-cloudwatch",
        ),
        opts=opts)

    amazon_cloudwatch_cloudwatch_agent_service_account = kubernetes.core.v1.ServiceAccount("fluent_bit_ServiceAccount",
        api_version="v1",
        kind="ServiceAccount",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="fluent-bit",
            namespace="amazon-cloudwatch",
        ),
        opts=opts)
    
    fluent_bit_role_cluster_role = kubernetes.rbac.v1.ClusterRole("fluent_bit_roleClusterRole",
        api_version="rbac.authorization.k8s.io/v1",
        kind="ClusterRole",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="fluent-bit-role",
        ),
        rules=[
            kubernetes.rbac.v1.PolicyRuleArgs(
                non_resource_urls=["/metrics"],
                verbs=["get"],
            ),
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=[""],
                resources=[
                    "namespaces",
                    "pods",
                    "pods/logs",
                ],
                verbs=[
                    "get",
                    "list",
                    "watch",
                ],
            ),
        ],
        opts=opts)

    fluent_bit_role_binding_cluster_role_binding = kubernetes.rbac.v1.ClusterRoleBinding("fluent_bit_role_bindingClusterRoleBinding",
        api_version="rbac.authorization.k8s.io/v1",
        kind="ClusterRoleBinding",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="fluent-bit-role-binding",
        ),
        role_ref=kubernetes.rbac.v1.RoleRefArgs(
            api_group="rbac.authorization.k8s.io",
            kind="ClusterRole",
            name="fluent-bit-role",
        ),
        subjects=[kubernetes.rbac.v1.SubjectArgs(
            kind="ServiceAccount",
            name="fluent-bit",
            namespace="amazon-cloudwatch",
        )],
        opts=opts)

    amazon_cloudwatch_fluent_bit_config_config_map = kubernetes.core.v1.ConfigMap("amazon_cloudwatchFluent_bit_configConfigMap",
        api_version="v1",
        kind="ConfigMap",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="fluent-bit-config",
            namespace="amazon-cloudwatch",
            labels={
                "k8s-app": "fluent-bit-compatible",
            },
        ),
        data={
            "fluent-bit.conf": """[SERVICE]
    Flush                     5
    Log_Level                 info
    Daemon                    off
    Parsers_File              parsers.conf
    HTTP_Server               ${HTTP_SERVER}
    HTTP_Listen               0.0.0.0
    HTTP_Port                 ${HTTP_PORT}
    storage.path              /var/fluent-bit/state/flb-storage/
    storage.sync              normal
    storage.checksum          off
    storage.backlog.mem_limit 5M
    
@INCLUDE application-log.conf
@INCLUDE dataplane-log.conf
@INCLUDE host-log.conf""",

            "application-log.conf": """[INPUT]
    Name                tail
    Tag                 application.*
    Exclude_Path        /var/log/containers/cloudwatch-agent*, /var/log/containers/fluent-bit*
    Path                /var/log/containers/*.log
    Docker_Mode         On
    Docker_Mode_Flush   5
    Docker_Mode_Parser  container_firstline
    Parser              docker
    DB                  /var/fluent-bit/state/flb_container.db
    Mem_Buf_Limit       50MB
    Skip_Long_Lines     On
    Refresh_Interval    10
    Rotate_Wait         30
    storage.type        filesystem
    Read_from_Head      ${READ_FROM_HEAD}

[INPUT]
    Name                tail
    Tag                 application.*
    Path                /var/log/containers/fluent-bit*
    Parser              docker
    DB                  /var/fluent-bit/state/flb_log.db
    Mem_Buf_Limit       5MB
    Skip_Long_Lines     On
    Refresh_Interval    10
    Read_from_Head      ${READ_FROM_HEAD}

[INPUT]
    Name                tail
    Tag                 application.*
    Path                /var/log/containers/cloudwatch-agent*
    Docker_Mode         On
    Docker_Mode_Flush   5
    Docker_Mode_Parser  cwagent_firstline
    Parser              docker
    DB                  /var/fluent-bit/state/flb_cwagent.db
    Mem_Buf_Limit       5MB
    Skip_Long_Lines     On
    Refresh_Interval    10
    Read_from_Head      ${READ_FROM_HEAD}

[FILTER]
    Name                kubernetes
    Match               application.*
    Kube_URL            https://kubernetes.default.svc:443
    Kube_Tag_Prefix     application.var.log.containers.
    Merge_Log           On
    Merge_Log_Key       log_processed
    K8S-Logging.Parser  On
    K8S-Logging.Exclude Off
    Annotations         Off

[FILTER]
    Name                nest
    Match               application.*
    Operation           lift
    Nested_under        kubernetes
    Add_prefix          Nested.

[FILTER]
    Name                modify
    Match               application.*
    Rename              Nested.docker_id            Docker.container_id

[FILTER]
    Name                nest
    Match               application.*
    Operation           nest
    Wildcard            Nested.*
    Nested_under        kubernetes
    Remove_prefix       Nested.

[FILTER]
    Name                nest
    Match               application.*
    Operation           nest
    Wildcard            Docker.*
    Nested_under        docker
    Remove_prefix       Docker.

[OUTPUT]
    Name                cloudwatch
    Match               application.*
    region              ${AWS_REGION}
    log_group_name      /aws/containerinsights/${CLUSTER_NAME}/application
    log_stream_name     $(tag[4])
    auto_create_group   true
    extra_user_agent    container-insights""",
            "dataplane-log.conf": """[INPUT]
    Name                systemd
    Tag                 dataplane.systemd.*
    Systemd_Filter      _SYSTEMD_UNIT=docker.service
    Systemd_Filter      _SYSTEMD_UNIT=kubelet.service
    DB                  /var/fluent-bit/state/systemd.db
    Path                /var/log/journal
    Read_From_Tail      ${READ_FROM_TAIL}

[FILTER]
    Name                modify
    Match               dataplane.systemd.*
    Rename              _HOSTNAME                   hostname
    Rename              _SYSTEMD_UNIT               systemd_unit
    Rename              MESSAGE                     message
    Remove_regex        ^((?!hostname|systemd_unit|message).)*$

[FILTER]
    Name                aws
    Match               dataplane.*
    imds_version        v1

[OUTPUT]
    Name                cloudwatch
    Match               dataplane.systemd.*
    region              ${AWS_REGION}
    log_group_name      /aws/containerinsights/${CLUSTER_NAME}/dataplane
    log_stream_name     $(tag[2]).$(tag[3])-$(hostname)
    auto_create_group   true
    extra_user_agent    container-insight""",
            "host-log.conf": """[INPUT]
    Name                tail
    Tag                 host.dmesg
    Path                /var/log/dmesg
    Parser              syslog
    DB                  /var/fluent-bit/state/flb_dmesg.db
    Mem_Buf_Limit       5MB
    Skip_Long_Lines     On
    Refresh_Interval    10
    Read_from_Head      ${READ_FROM_HEAD}

[INPUT]
    Name                tail
    Tag                 host.messages
    Path                /var/log/messages
    Parser              syslog
    DB                  /var/fluent-bit/state/flb_messages.db
    Mem_Buf_Limit       5MB
    Skip_Long_Lines     On
    Refresh_Interval    10
    Read_from_Head      ${READ_FROM_HEAD}

[INPUT]
    Name                tail
    Tag                 host.secure
    Path                /var/log/secure
    Parser              syslog
    DB                  /var/fluent-bit/state/flb_secure.db
    Mem_Buf_Limit       5MB
    Skip_Long_Lines     On
    Refresh_Interval    10
    Read_from_Head      ${READ_FROM_HEAD}

[FILTER]
    Name                aws
    Match               host.*
    imds_version        v1

[OUTPUT]
    Name                cloudwatch
    Match               host.*
    region              ${AWS_REGION}
    log_group_name      /aws/containerinsights/${CLUSTER_NAME}/host
    log_stream_name     $(tag)-$(host)
    auto_create_group   true
    extra_user_agent    container-insights""",
            "parsers.conf": """[PARSER]
    Name                docker
    Format              json
    Time_Key            time
    Time_Format         %Y-%m-%dT%H:%M:%S.%LZ

[PARSER]
    Name                syslog
    Format              regex
    Regex               ^(?<time>[^ ]* {1,2}[^ ]* [^ ]*) (?<host>[^ ]*) (?<ident>[a-zA-Z0-9_\/\.\-]*)(?:\[(?<pid>[0-9]+)\])?(?:[^\:]*\:)? *(?<message>.*)$
    Time_Key            time
    Time_Format         %b %d %H:%M:%S

[PARSER]
    Name                container_firstline
    Format              regex
    Regex               (?<log>(?<="log":")\S(?!\.).*?)(?<!\\)".*(?<stream>(?<="stream":").*?)".*(?<time>\d{4}-\d{1,2}-\d{1,2}T\d{2}:\d{2}:\d{2}\.\w*).*(?=})
    Time_Key            time
    Time_Format         %Y-%m-%dT%H:%M:%S.%LZ

[PARSER]
    Name                cwagent_firstline
    Format              regex
    Regex               (?<log>(?<="log":")\d{4}[\/-]\d{1,2}[\/-]\d{1,2}[ T]\d{2}:\d{2}:\d{2}(?!\.).*?)(?<!\\)".*(?<stream>(?<="stream":").*?)".*(?<time>\d{4}-\d{1,2}-\d{1,2}T\d{2}:\d{2}:\d{2}\.\w*).*(?=})
    Time_Key            time
    Time_Format         %Y-%m-%dT%H:%M:%S.%LZ"""
        },
        opts=opts)

    amazon_cloudwatch_fluent_bit_compatible_daemon_set = kubernetes.apps.v1.DaemonSet("amazon_cloudwatchFluent_bit_compatibleDaemonSet",
        api_version="apps/v1",
        kind="DaemonSet",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="fluent-bit-compatible",
            namespace="amazon-cloudwatch",
            labels={
                "k8s-app": "fluent-bit-compatible",
                "version": "v1",
                "kubernetes.io/cluster-service": "true",
            },
        ),
        spec=kubernetes.apps.v1.DaemonSetSpecArgs(
            selector=kubernetes.meta.v1.LabelSelectorArgs(
                match_labels={
                    "k8s-app": "fluent-bit-compatible",
                },
            ),
            template=kubernetes.core.v1.PodTemplateSpecArgs(
                metadata=kubernetes.meta.v1.ObjectMetaArgs(
                    labels={
                        "k8s-app": "fluent-bit-compatible",
                        "version": "v1",
                        "kubernetes.io/cluster-service": "true",
                    },
                ),
                spec=kubernetes.core.v1.PodSpecArgs(
                    containers=[kubernetes.core.v1.ContainerArgs(
                        name="fluent-bit-compatible",
                        image="public.ecr.aws/aws-observability/aws-for-fluent-bit:stable",
                        image_pull_policy="Always",
                        env=[
                            kubernetes.core.v1.EnvVarArgs(
                                name="AWS_REGION",
                                value_from={
                                    "config_map_key_ref": {
                                        "name": "fluent-bit-cluster-info",
                                        "key": "logs.region",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="CLUSTER_NAME",
                                value_from={
                                    "config_map_key_ref": {
                                        "name": "fluent-bit-cluster-info",
                                        "key": "cluster.name",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="HTTP_SERVER",
                                value_from={
                                    "config_map_key_ref": {
                                        "name": "fluent-bit-cluster-info",
                                        "key": "http.server",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="HTTP_PORT",
                                value_from={
                                    "config_map_key_ref": {
                                        "name": "fluent-bit-cluster-info",
                                        "key": "http.port",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="READ_FROM_HEAD",
                                value_from={
                                    "config_map_key_ref": {
                                        "name": "fluent-bit-cluster-info",
                                        "key": "read.head",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="READ_FROM_TAIL",
                                value_from={
                                    "config_map_key_ref": {
                                        "name": "fluent-bit-cluster-info",
                                        "key": "read.tail",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="HOST_NAME",
                                value_from={
                                    "field_ref": {
                                        "field_path": "spec.nodeName",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="CI_VERSION",
                                value="k8s/1.3.11",
                            ),
                        ],
                        resources=kubernetes.core.v1.ResourceRequirementsArgs(
                            limits={
                                "memory": "200Mi",
                            },
                            requests={
                                "cpu": "500m",
                                "memory": "100Mi",
                            },
                        ),
                        volume_mounts=[
                            {
                                "name": "fluentbitstate",
                                "mount_path": "/var/fluent-bit/state",
                            },
                            {
                                "name": "varlog",
                                "mount_path": "/var/log",
                                "read_only": True,
                            },
                            {
                                "name": "varlibdockercontainers",
                                "mount_path": "/var/lib/docker/containers",
                                "read_only": True,
                            },
                            {
                                "name": "fluent-bit-config",
                                "mount_path": "/fluent-bit/etc/",
                            },
                            {
                                "name": "runlogjournal",
                                "mount_path": "/run/log/journal",
                                "read_only": True,
                            },
                            {
                                "name": "dmesg",
                                "mount_path": "/var/log/dmesg",
                                "read_only": True,
                            },
                        ],
                    )],
                    termination_grace_period_seconds=10,
                    volumes=[
                        kubernetes.core.v1.VolumeArgs(
                            name="fluentbitstate",
                            host_path={
                                "path": "/var/fluent-bit/state",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="varlog",
                            host_path={
                                "path": "/var/log",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="varlibdockercontainers",
                            host_path={
                                "path": "/var/lib/docker/containers",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="fluent-bit-config",
                            config_map={
                                "name": "fluent-bit-config",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="runlogjournal",
                            host_path={
                                "path": "/run/log/journal",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="dmesg",
                            host_path={
                                "path": "/var/log/dmesg",
                            },
                        ),
                    ],
                    service_account_name="fluent-bit",
                    tolerations=[
                        kubernetes.core.v1.TolerationArgs(
                            key="node-role.kubernetes.io/master",
                            operator="Exists",
                            effect="NoSchedule",
                        ),
                        kubernetes.core.v1.TolerationArgs(
                            operator="Exists",
                            effect="NoExecute",
                        ),
                        kubernetes.core.v1.TolerationArgs(
                            operator="Exists",
                            effect="NoSchedule",
                        ),
                    ],
                ),
            ),
        ), 
        opts=opts)