from pulumi import ResourceOptions
from pulumi_eks import Cluster
import pulumi_kubernetes as kubernetes

# L'agent CloudWatch est responsable du groupe de journaux performance disponible sur cloudwatch
# Source: https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/latest/k8s-deployment-manifest-templates/deployment-mode/daemonset/container-insights-monitoring/quickstart/cwagent-fluentd-quickstart.yaml
# Traduit avec https://www.pulumi.com/kube2pulumi/
def CloudWatchAgent(cluster: Cluster, opts: ResourceOptions):
    amazon_cloudwatch_cloudwatch_agent_service_account = kubernetes.core.v1.ServiceAccount("amazon_cloudwatchCloudwatch_agentServiceAccount",
        api_version="v1",
        kind="ServiceAccount",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="cloudwatch-agent",
            namespace="amazon-cloudwatch",
        ),
        opts=opts)

    cloudwatch_agent_role_cluster_role = kubernetes.rbac.v1.ClusterRole("cloudwatch_agent_roleClusterRole",
        kind="ClusterRole",
        api_version="rbac.authorization.k8s.io/v1",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="cloudwatch-agent-role",
        ),
        rules=[
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=[""],
                resources=[
                    "pods",
                    "nodes",
                    "endpoints",
                ],
                verbs=[
                    "list",
                    "watch",
                ],
            ),
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=["apps"],
                resources=["replicasets"],
                verbs=[
                    "list",
                    "watch",
                ],
            ),
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=["batch"],
                resources=["jobs"],
                verbs=[
                    "list",
                    "watch",
                ],
            ),
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=[""],
                resources=["nodes/proxy"],
                verbs=["get"],
            ),
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=[""],
                resources=[
                    "nodes/stats",
                    "configmaps",
                    "events",
                ],
                verbs=["create"],
            ),
            kubernetes.rbac.v1.PolicyRuleArgs(
                api_groups=[""],
                resources=["configmaps"],
                resource_names=["cwagent-clusterleader"],
                verbs=[
                    "get",
                    "update",
                ],
            ),
        ],
        opts=opts)

    cloudwatch_agent_role_binding_cluster_role_binding = kubernetes.rbac.v1.ClusterRoleBinding("cloudwatch_agent_role_bindingClusterRoleBinding",
        kind="ClusterRoleBinding",
        api_version="rbac.authorization.k8s.io/v1",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="cloudwatch-agent-role-binding",
        ),
        subjects=[kubernetes.rbac.v1.SubjectArgs(
            kind="ServiceAccount",
            name="cloudwatch-agent",
            namespace="amazon-cloudwatch",
        )],
        role_ref=kubernetes.rbac.v1.RoleRefArgs(
            kind="ClusterRole",
            name="cloudwatch-agent-role",
            api_group="rbac.authorization.k8s.io",
        ),
        opts=opts)

    amazon_cloudwatch_cwagentconfig_config_map = kubernetes.core.v1.ConfigMap("amazon_cloudwatchCwagentconfigConfigMap",
        api_version="v1",
        data={
            "cwagentconfig.json": cluster.core.cluster.name.apply(lambda name: f"""{{
                "agent": {{
                    "region": "ca-central-1"
                }},
                "logs": {{
                    "metrics_collected": {{
                    "kubernetes": {{
                        "cluster_name": "{name}",
                        "metrics_collection_interval": 60
                        }}
                    }},
                    "force_flush_interval": 5
                    }}
                }}
            """)
        },
        kind="ConfigMap",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="cwagentconfig",
            namespace="amazon-cloudwatch",
        ),
        opts=opts)

    amazon_cloudwatch_cloudwatch_agent_daemon_set = kubernetes.apps.v1.DaemonSet("amazon_cloudwatchCloudwatch_agentDaemonSet",
        api_version="apps/v1",
        kind="DaemonSet",
        metadata=kubernetes.meta.v1.ObjectMetaArgs(
            name="cloudwatch-agent",
            namespace="amazon-cloudwatch",
        ),
        spec=kubernetes.apps.v1.DaemonSetSpecArgs(
            selector=kubernetes.meta.v1.LabelSelectorArgs(
                match_labels={
                    "name": "cloudwatch-agent",
                },
            ),
            template=kubernetes.core.v1.PodTemplateSpecArgs(
                metadata=kubernetes.meta.v1.ObjectMetaArgs(
                    labels={
                        "name": "cloudwatch-agent",
                    },
                ),
                spec=kubernetes.core.v1.PodSpecArgs(
                    containers=[kubernetes.core.v1.ContainerArgs(
                        name="cloudwatch-agent",
                        image="amazon/cloudwatch-agent:1.247354.0b251981",
                        resources=kubernetes.core.v1.ResourceRequirementsArgs(
                            limits={
                                "cpu": "200m",
                                "memory": "200Mi",
                            },
                            requests={
                                "cpu": "200m",
                                "memory": "200Mi",
                            },
                        ),
                        env=[
                            kubernetes.core.v1.EnvVarArgs(
                                name="HOST_IP",
                                value_from={
                                    "field_ref": {
                                        "field_path": "status.hostIP",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="HOST_NAME",
                                value_from={
                                    "field_ref": {
                                        "field_path": "spec.nodeName",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="K8S_NAMESPACE",
                                value_from={
                                    "field_ref": {
                                        "field_path": "metadata.namespace",
                                    },
                                },
                            ),
                            kubernetes.core.v1.EnvVarArgs(
                                name="CI_VERSION",
                                value="k8s/1.3.11",
                            ),
                        ],
                        volume_mounts=[
                            {
                                "name": "cwagentconfig",
                                "mount_path": "/etc/cwagentconfig",
                            },
                            {
                                "name": "rootfs",
                                "mount_path": "/rootfs",
                                "read_only": True,
                            },
                            {
                                "name": "dockersock",
                                "mount_path": "/var/run/docker.sock",
                                "read_only": True,
                            },
                            {
                                "name": "varlibdocker",
                                "mount_path": "/var/lib/docker",
                                "read_only": True,
                            },
                            {
                                "name": "containerdsock",
                                "mount_path": "/run/containerd/containerd.sock",
                                "read_only": True,
                            },
                            {
                                "name": "sys",
                                "mount_path": "/sys",
                                "read_only": True,
                            },
                            {
                                "name": "devdisk",
                                "mount_path": "/dev/disk",
                                "read_only": True,
                            },
                        ],
                    )],
                    volumes=[
                        kubernetes.core.v1.VolumeArgs(
                            name="cwagentconfig",
                            config_map={
                                "name": "cwagentconfig",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="rootfs",
                            host_path={
                                "path": "/",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="dockersock",
                            host_path={
                                "path": "/var/run/docker.sock",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="varlibdocker",
                            host_path={
                                "path": "/var/lib/docker",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="containerdsock",
                            host_path={
                                "path": "/run/containerd/containerd.sock",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="sys",
                            host_path={
                                "path": "/sys",
                            },
                        ),
                        kubernetes.core.v1.VolumeArgs(
                            name="devdisk",
                            host_path={
                                "path": "/dev/disk/",
                            },
                        ),
                    ],
                    termination_grace_period_seconds=60,
                    service_account_name="cloudwatch-agent",
                ),
            ),
        ),
        opts=opts)