# EKS Cloudwatch pulumi

This is a Pulumi to deploy a eks cluster with cloud watch enabled.

## Virtual env

Active the virtual env on windows

```powershell
.\venv\Scripts\Activate.ps1
```

## Install dependencies

Using chocolaty

```powershell
choco install -y pulumi
choco install -y aws-iam-authenticator
```

## Deploy the cluster

```powershell
pulumi up
```

## Get the kubeconfig

```powershell
aws eks update-kubeconfig --name (pulumi stack output cluster_name)
```

## Cleanup resources

```powershell
pulumi destroy -y
```