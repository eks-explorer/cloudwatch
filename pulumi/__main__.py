import json
import pulumi
import pulumi_eks as eks
import pulumi_kubernetes as k8s

from cloud_watch import CloudWatch

# Create an EKS cluster with default configuration.
cluster = eks.Cluster("eks-cloudwatch-pulumi")

# Create a Kubernetes provider instance that uses our cluster from above.
provider = k8s.Provider("eks-provider", kubeconfig=cluster.kubeconfig.apply(lambda k: json.dumps(k)))

# Deploy erabliereapi/erabliereapi to the cluster using Deployment and Service in the namespace erabliereapi.
namespace = k8s.core.v1.Namespace("erabliereapi",
    metadata={
        "name": "erabliereapi",
    }, 
    opts=pulumi.ResourceOptions(provider=provider)
)

deployment = k8s.apps.v1.Deployment("erabliereapi",
    metadata={
        "namespace": namespace.metadata["name"],
        "labels": {
            "app": "erabliereapi",
        },
    },
    spec={
        "replicas": 1,
        "selector": {
            "match_labels": {
                "app": "erabliereapi",
            },
        },
        "template": {
            "metadata": {
                "labels": {
                    "app": "erabliereapi",
                },
            },
            "spec": {
                "containers": [{
                    "name": "erabliereapi",
                    "image": "erabliereapi/erabliereapi:v3-dev",
                    "ports": [{
                        "containerPort": 80,
                    }],
                    "env": [{
                        "name": "ASPNETCORE_ENVIRONMENT",
                        "value": "Development",
                    }]
                }],
            },
        },
    },
    opts=pulumi.ResourceOptions(provider=provider))

service = k8s.core.v1.Service("erabliereapi",
    metadata={
        "namespace": namespace.metadata["name"],
        "labels": deployment.spec["template"]["metadata"]["labels"],
    },
    spec={
        "ports": [{
            "port": 80,
            "targetPort": 80,
        }],
        "selector": deployment.spec["template"]["metadata"]["labels"],
        "type": "LoadBalancer",
    },
    opts=pulumi.ResourceOptions(provider=provider))

pulumi.export("service_ip", service.status["load_balancer"]["ingress"][0]["hostname"])

pulumi.export("role_arn", cluster.instance_roles.apply(lambda r: r[0].arn))

pulumi.export("cluster_name", cluster.core.cluster.name)

# Deploy cloud watch ressources
cloudwatch = CloudWatch(cluster=cluster, opts=pulumi.ResourceOptions(provider=provider))