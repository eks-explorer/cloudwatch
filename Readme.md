# EKS - Cloud watch

### Create a cluster

```
eksctl create cluster -n test-cloud-watch -N 1
eksctl create nodegroup --cluster test-cloud-watch
```

### Get the kubeconfig file

```
aws eks update-kubeconfig --name test-cloud-watch
```

### Deploy wordpress

```
# Create a namespace wordpress
kubectl create namespace wordpress-cwi

# Add the bitnami Helm Charts Repository
helm repo add bitnami https://charts.bitnami.com/bitnami

# Deploy WordPress in its own namespace
helm -n wordpress-cwi install understood-zebu bitnami/wordpress
```

The following output will occure

```
NAME: understood-zebu
LAST DEPLOYED: Sun Dec 18 14:16:56 2022
NAMESPACE: wordpress-cwi
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: wordpress
CHART VERSION: 15.2.20
APP VERSION: 6.1.1

** Please be patient while the chart is being deployed **

Your WordPress site can be accessed through the following DNS name from within your cluster:

    understood-zebu-wordpress.wordpress-cwi.svc.cluster.local (port 80)

To access your WordPress site from outside the cluster follow the steps below:

1. Get the WordPress URL by running these commands:

  NOTE: It may take a few minutes for the LoadBalancer IP to be available.
        Watch the status with: 'kubectl get svc --namespace wordpress-cwi -w understood-zebu-wordpress'

   export SERVICE_IP=$(kubectl get svc --namespace wordpress-cwi understood-zebu-wordpress --include "{{ range (index .status.loadBalancer.ingress 0) }}{{ . }}{{ end }}")
   echo "WordPress URL: http://$SERVICE_IP/"
   echo "WordPress Admin URL: http://$SERVICE_IP/admin"

2. Open a browser and access WordPress using the obtained URL.

3. Login with the following credentials below to see your blog:

  echo Username: user
  echo Password: $(kubectl get secret --namespace wordpress-cwi understood-zebu-wordpress -o jsonpath="{.data.wordpress-password}" | base64 -d)
```

Follow the wordpress deployment

```
kubectl -n wordpress-cwi rollout status deployment understood-zebu-wordpress
```

### Get the wordpress url

```
kubectl get svc -n wordpress-cwi understood-zebu-wordpress --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}"
```

#### Admin dashboard

The username is user

and the password can be found with the following command

```
kubectl get secret --namespace wordpress-cwi understood-zebu-wordpress -o jsonpath="{.data.wordpress-password}"
```

Then decode the base64 string to get the real password.

### Get the role name

First get the stackname

```
eksctl get nodegroup --cluster test-cloud-watch -o json
```

Then get the role name

```
aws cloudformation describe-stack-resources --stack-name eksctl-test-cloud-watch-nodegroup-ng-1a26864c
```

The role name is the value of the key PhysicalResourceId of the object with ResourceType AWS::IAM::Role

### Attach the policy

Attach the policy
```
aws iam attach-role-policy --role-name eksctl-test-cloud-watch-nodegroup-NodeInstanceRole-1WU8ZUZGZLZJ3 --policy-arn arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy
```

Veirfy the policy is attached

```
aws iam list-attached-role-policies --role-name eksctl-test-cloud-watch-nodegroup-NodeInstanceRole-1WU8ZUZGZLZJ3
```

### Installing container insight

```
kubectl apply -f cloud-watch.yaml
```

Just wait a few minutes and the metrics will be available in the cloud watch console

https://ca-central-1.console.aws.amazon.com/cloudwatch/home?region=ca-central-1#container-insights:performance/EKS:Service?~(query~(controls~(CW*3a*3aEKS.cluster~(~'test-cloud-watch)))~context~())%22

### Delete the cluster

```
eksctl delete cluster -n test-cloud-watch
```

## References

- https://www.eksworkshop.com/intermediate/250_cloudwatch_container_insights/gettingstarted/
